import core.sys.windows.windows, core.sys.windows.dll;
import std.concurrency : spawn, yield;
import core.runtime;

import llmo;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID) {
	final switch (ulReason) {
	case DLL_PROCESS_ATTACH:
		Runtime.initialize();
		spawn({
			while (*cast(uint*)0xC8D4C0 < 9)
				yield();
			init();
		});
		dll_process_attach(hInstance, true);
		break;
	case DLL_PROCESS_DETACH:
		uninit();
		Runtime.terminate();
		dll_process_detach(hInstance, true);
		break;
	case DLL_THREAD_ATTACH:
		dll_thread_attach(true, true);
		break;
	case DLL_THREAD_DETACH:
		dll_thread_detach(true, true);
		break;
	}
	return true;
}

/// Uninitialize plugin: restore original code
void uninit() {
	writeMemory(cast(ubyte*)0x569672, cast(ubyte)0x00);
	writeMemory(cast(ubyte*)0x5696D4, cast(ubyte)0x00);
	writeMemory(cast(ubyte*)0x5696D6, cast(ubyte)0x00);
}

/// Initialize plugin: set hacks
void init() {
	writeMemory(cast(ubyte*)0x569672, cast(ubyte)0x01);
	writeMemory(cast(ubyte*)0x5696D4, cast(ubyte)0x01);
	writeMemory(cast(ubyte*)0x5696D6, cast(ubyte)0x01);
}
